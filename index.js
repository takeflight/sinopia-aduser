var HTPasswd = require('sinopia-htpasswd');
var path = require('path')

function addUser(filename, user, password, cb) {
	var config = {'file': filename, 'max_users': Infinity};
	var sinopiaConfig = {'config': {
		'logger': null, // Seems it is not even used?
		'self_path': path.join(process.cwd(), 'bogus'), // Dont ask...
	}};
	var htpasswd = HTPasswd(config, sinopiaConfig);
	htpasswd.adduser(user, password, cb);
}

module.exports = addUser;
